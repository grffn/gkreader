
var Document,
    currentPage,
    container,
    width,
    marginRight,
    numberOfPages,
    paper,
    text;

window.onload = function(){
    container = document.getElementById("gkreader_container");
    width = window.innerWidth;
    var style = container.currentStyle || window.getComputedStyle(document.getElementById("gkreader_wrapper"));
    marginRight = parseInt(style.paddingRight.slice(0, -2));
    container.style.webkitColumnWidth = "" + width + "px";
    currentPage = 0;
    numberOfPages = (container.scrollWidth / width) >> 0;
    var header = document.getElementById("gkreader_header");
    paper = Raphael(header, header.innerWidth, header.innerHeight);
    setPage(0);

}

function nextPage() {
    setPage(currentPage + 1);
}

function previousPage() {
    setPage(currentPage - 1);
}

function setPage(pageNum) {
    if (pageNum < 0 || pageNum >= numberOfPages) {
        return;
    }
    container.style.right = (width - marginRight) * pageNum + marginRight ;
    currentPage = pageNum;
    setPageText();
    var currentElement = document.elementFromPoint(marginRight, container.offsetTop);
    var position = [];
    while (currentElement.parentElement.tagName!=="BODY"){
    position.push(getNodeIndex(currentElement));
    }
    Document.savePosition(JSON.stringify(pos));
}

function setPageText() {
    if (!text) {
        text = paper.text(20, 10, "");
        text.attr({"font-size": 14});
    }
    text.attr({
        "text": getCurrentPageNumber() + "/" + getNumberOfPages()
    });
}

function scrollToElementNum(num){

}

function getNumberOfPages() {
    return numberOfPages;
}

function getCurrentPageNumber() {
    return currentPage + 1;
}

function getNodeIndex(node) {
    var index = 0;
    while ( (node = node.previousSibling) ) {
        if (node.nodeType != 3 || !/^\s*$/.test(node.data)) {
            index++;
        }
    }
    return index;
}