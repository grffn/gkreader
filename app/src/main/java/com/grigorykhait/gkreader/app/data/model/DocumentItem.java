package com.grigorykhait.gkreader.app.data.model;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

import com.grigorykhait.gkreader.app.data.DocumentContract;
import com.grigorykhait.gkreader.app.data.DocumentStorageDatabase;
import com.grigorykhait.gkreader.app.utils.Utils;
import com.grigorykhait.gkreader.logic.documents.Document;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.grigorykhait.gkreader.app.data.DocumentContract.DocumentColumns.COVER;
import static com.grigorykhait.gkreader.app.data.DocumentContract.DocumentColumns.DESCRIPTION;
import static com.grigorykhait.gkreader.app.data.DocumentContract.DocumentColumns.FILE_URI;
import static com.grigorykhait.gkreader.app.data.DocumentContract.DocumentColumns.LANG;
import static com.grigorykhait.gkreader.app.data.DocumentContract.DocumentColumns.LAST_MODIFIED;
import static com.grigorykhait.gkreader.app.data.DocumentContract.DocumentColumns.LAST_VIEWED;
import static com.grigorykhait.gkreader.app.data.DocumentContract.DocumentColumns.MIME_TYPE;
import static com.grigorykhait.gkreader.app.data.DocumentContract.DocumentColumns.TITLE;

/**
 * Created by grffn on 07.04.2015.
 */
public class DocumentItem {
    private static final String LOG_TAG = DocumentItem.class.getSimpleName();
    public long id = -1;
    public String title;
    public String description;
    public String mimeType;
    public String fileUri;
    public String coverPath;
    public String lang;
    public Date lastViewed;
    public Date lastModified;
    public Bitmap cover;

    public ArrayList<AuthorItem> authors;
    public ArrayList<SeriesItem> series;
    public ArrayList<GenreItem> genres;

    protected DocumentItem() {
    }

    public static DocumentItem createFromCursor(Cursor cursor) {
        DocumentItem doc = new DocumentItem();
        int index = cursor.getColumnIndex(BaseColumns._ID);
        if (index != -1) {
            doc.id = cursor.getLong(index);
        }
        index = cursor.getColumnIndex(TITLE);
        if (index != -1)
            doc.title = cursor.getString(index);
        index = cursor.getColumnIndex(DESCRIPTION);
        if (index != -1)
            doc.description = cursor.getString(index);
        index = cursor.getColumnIndex(MIME_TYPE);
        if (index != -1)
            doc.mimeType = cursor.getString(index);
        index = cursor.getColumnIndex(FILE_URI);
        if (index != -1)
            doc.fileUri = cursor.getString(index);
        index = cursor.getColumnIndex(COVER);
        if (index != -1)
            doc.coverPath = cursor.getString(index);
        index = cursor.getColumnIndex(LANG);
        if (index != -1)
            doc.lang = cursor.getString(index);
        index = cursor.getColumnIndex(LAST_VIEWED);
        if (index != -1) {
            long last_viewed = cursor.getLong(index);
            if (last_viewed != 0L) {
                doc.lastViewed = new Date(last_viewed);
            }
        }
        index = cursor.getColumnIndex(LAST_MODIFIED);
        if (index != -1) {
            long last_modified = cursor.getLong(index);
            if (last_modified != 0L) {
                doc.lastModified = new Date(last_modified);
            }
        }
        index = cursor.getColumnIndex(DocumentContract.DocumentsFullQuery.GENRES);
        if (index != -1) {
            String genresString = cursor.getString(index);
            doc.genres = parseGenres(genresString);
        }
        index = cursor.getColumnIndex(DocumentContract.DocumentsFullQuery.AUTHORS);
        if (index != -1) {
            String authorsString = cursor.getString(index);
            doc.authors = parseAuthors(authorsString);
        }
        index = cursor.getColumnIndex(DocumentContract.DocumentsFullQuery.SERIES);
        if (index != -1) {
            String seriesString = cursor.getString(index);
            doc.series = parseSeries(seriesString);
        }
        return doc;
    }

    public static DocumentItem createFromDocument(Document document) {
        DocumentItem item = new DocumentItem();
        item.fileUri = document.getFile().getPath();
        item.title = document.getTitle();
        item.cover = document.getCover();
        item.description = document.getAnnotation();
        item.lang = document.getLanguageCode();
        item.authors = toAuthorList(document.getAuthors());
        item.genres = toGenreList(document.getGenres());
        //item.series = toSeriesList(document.getSeries());
        return item;
    }

    public static DocumentItem createFromDocument(Document document, long id) {
        DocumentItem item = createFromDocument(document);
        item.id = id;
        return item;
    }

    private static ArrayList<AuthorItem> parseAuthors(String authorsString) {
        final String pattern = "\\{(\\d+),(.+?)\\}";
        ArrayList<AuthorItem> items = new ArrayList<>();
        if (authorsString == null) return items;
        Matcher matcher = Pattern.compile(pattern).matcher(authorsString);
        while (matcher.find()) {
            AuthorItem item = new AuthorItem();
            item.id = Long.decode(matcher.group(1));
            item.name = matcher.group(2);
            items.add(item);
        }
        return items;
    }

    private static ArrayList<GenreItem> parseGenres(String authorsString) {
        final String pattern = "\\{(\\d+),(.+?)\\}";
        ArrayList<GenreItem> items = new ArrayList<>();
        if (authorsString == null) return items;
        Matcher matcher = Pattern.compile(pattern).matcher(authorsString);
        while (matcher.find()) {
            GenreItem item = new GenreItem();
            item.id = Long.decode(matcher.group(1));
            item.name = matcher.group(2);
            items.add(item);
        }
        return items;
    }

    private static ArrayList<SeriesItem> parseSeries(String authorsString) {
        final String pattern = "\\{(\\d+),(.+?)\\}";
        ArrayList<SeriesItem> items = new ArrayList<>();
        if (authorsString == null) return items;
        Matcher matcher = Pattern.compile(pattern).matcher(authorsString);
        while (matcher.find()) {
            SeriesItem item = new SeriesItem();
            item.id = Long.decode(matcher.group(1));
            item.title = matcher.group(2);
            items.add(item);
        }
        return items;
    }

    private static ArrayList<AuthorItem> toAuthorList(List<String> strings) {
        ArrayList<AuthorItem> items = new ArrayList<>();
        if (strings != null) {
            for (String string : strings) {
                AuthorItem item = new AuthorItem();
                item.name = string;
                items.add(item);
            }
        }
        return items;
    }

    private static ArrayList<GenreItem> toGenreList(List<String> strings) {
        ArrayList<GenreItem> items = new ArrayList<>();
        if (strings != null) {
            for (String string : strings) {
                GenreItem item = new GenreItem();
                item.name = string;
                items.add(item);
            }
        }
        return items;
    }

    private static ArrayList<SeriesItem> toSeriesList(List<String> strings) {
        ArrayList<SeriesItem> items = new ArrayList<>();
        if (strings != null) {
            for (String string : strings) {
                SeriesItem item = new SeriesItem();
                item.title = string;
                items.add(item);
            }
        }
        return items;
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        if (title != null) {
            contentValues.put(DocumentContract.DocumentColumns.TITLE, title);
        }
        if (description != null) {
            contentValues.put(DocumentContract.DocumentColumns.DESCRIPTION, description);
        }
        if (mimeType != null) {
            contentValues.put(DocumentContract.DocumentColumns.MIME_TYPE, mimeType);
        }
        if (fileUri != null) {
            contentValues.put(DocumentContract.DocumentColumns.FILE_URI, fileUri);
            contentValues.put(DocumentContract.DocumentColumns.LAST_MODIFIED, new File(fileUri).lastModified());
        }
        if (lang != null) {
            contentValues.put(DocumentContract.DocumentColumns.LANG, lang);
        }
        if (coverPath != null) {
            contentValues.put(DocumentContract.DocumentColumns.COVER, coverPath);
        }
        //contentValues.put(DocumentContract.DocumentColumns.LAST_MODIFIED, lastModified.getTime());
//        if (lastViewed != null) {
//            contentValues.put(DocumentContract.DocumentColumns.LAST_VIEWED, lastViewed.getTime());
//        }
        return contentValues;
    }

    public void save(Context context) {
        if (cover != null) {
            if (coverPath != null && !coverPath.equals("")) {
                coverPath = Utils.saveImage(context, cover, coverPath);
            } else {
                coverPath = Utils.saveImage(context, cover);
            }
        }
        ContentValues contentValues = getContentValues();
        ContentResolver resolver = context.getContentResolver();
        if (id == -1) {
            Uri uri = resolver.insert(DocumentContract.Document.CONTENT_URI, contentValues);
            id = ContentUris.parseId(uri);
        } else {
            resolver.update(DocumentContract.Document.CONTENT_URI, contentValues, "_id=?",
                    new String[]{String.valueOf(id)});
        }
        ArrayList<ContentValues> valuesList = new ArrayList<>();
        for (AuthorItem item : authors) {
            try {
                if (item.id == -1) {
                    Cursor authors = resolver.query(DocumentContract.Author.buildAuthorWithName(item.name),
                            new String[]{DocumentContract.Author._ID},
                            null, null, null);
                    if (authors.getCount() == 0) {
                        ContentValues authorValues = new ContentValues();
                        authorValues.put(DocumentContract.AuthorColumns.NAME, item.name);
                        Uri uri = resolver.insert(DocumentContract.Author.CONTENT_URI, authorValues);
                        item.id = ContentUris.parseId(uri);
                    } else {
                        authors.moveToNext();
                        item.id = authors.getLong(0);
                    }
                    authors.close();
                }
                ContentValues cv = new ContentValues();
                cv.put(DocumentStorageDatabase.DocumentsAuthors.AUTHOR_ID, item.id);
                cv.put(DocumentStorageDatabase.DocumentsAuthors.DOCUMENT_ID, id);
                valuesList.add(cv);
            } catch (SQLException e) {
                Log.e(LOG_TAG, "item link was not inserted");
            }
        }
        resolver.bulkInsert(DocumentStorageDatabase.DocumentsAuthors.CONTENT_URI,
                valuesList.toArray(new ContentValues[valuesList.size()]));
    }

    public void remove(Context context) {
        ContentResolver resolver = context.getContentResolver();
        resolver.delete(DocumentContract.Document.CONTENT_URI, "_id=?", new String[]{String.valueOf(id)});
        if (coverPath != null && !"".equals(coverPath)) {
            Utils.deleteImage(context, coverPath);
        }
    }

    public String getAuthorsString() {
        if (authors == null) {
            return "";
        }

        StringBuilder builder = new StringBuilder();
        Iterator<AuthorItem> iterator = authors.iterator();
        while (iterator.hasNext()) {
            AuthorItem author = iterator.next();
            builder.append(author.name);
            if (iterator.hasNext()) {
                builder.append(", ");
            }
        }
        return builder.toString();
    }
}
