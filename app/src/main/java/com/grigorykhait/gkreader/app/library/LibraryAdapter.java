package com.grigorykhait.gkreader.app.library;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.grigorykhait.gkreader.R;
import com.grigorykhait.gkreader.app.ReaderActivity;
import com.grigorykhait.gkreader.app.data.model.DocumentItem;
import com.grigorykhait.gkreader.app.utils.Utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class LibraryAdapter extends RecyclerView.Adapter<LibraryAdapter.DocumentViewHolder> {

    private final Context mContext;
    List<DocumentItem> mDocuments;

    public LibraryAdapter(Context context) {
        mDocuments = new ArrayList<>();
        mContext = context;
    }

    @Override
    public DocumentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.library_document_item_view, parent, false);
        // set the view's size, margins, paddings and layout parameters
        return new DocumentViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DocumentViewHolder holder, int position) {
        DocumentItem item = mDocuments.get(position);
        holder.setDocument(item);
    }

    @Override
    public int getItemCount() {
        return mDocuments.size();
    }

    public class DocumentViewHolder extends RecyclerView.ViewHolder {

        private TextView mTitleView;
        private TextView mAuthorsView;
        private ImageView mCoverView;
        private ImageView mReadAction;

        private DocumentItem mDocument;


        public DocumentViewHolder(View itemView) {
            super(itemView);
            mTitleView = (TextView) itemView.findViewById(R.id.document_item_title);
            mAuthorsView = (TextView) itemView.findViewById(R.id.document_item_authors);
            mCoverView = (ImageView) itemView.findViewById(R.id.document_item_cover);
            mReadAction = (ImageView) itemView.findViewById(R.id.document_item_action_read);

        }

        public void setDocument(DocumentItem item) {
            mDocument = item;
            mTitleView.setText(item.title);

            mAuthorsView.setText(item.getAuthorsString());
            if (item.coverPath != null) {
                mCoverView.setImageBitmap(Utils.getImage(item.coverPath));
            }
            mReadAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mDocument == null) {
                        return;
                    }
                    Intent intent = new Intent(mContext, ReaderActivity.class);
                    intent.setData(Uri.parse(mDocument.fileUri));
                    mContext.startActivity(intent);
                }
            });
        }
    }

    public void clear() {
        int size = mDocuments.size();
        mDocuments.clear();
        this.notifyItemRangeRemoved(0, size - 1);
    }

    public void add(DocumentItem item) {
        mDocuments.add(item);
        this.notifyItemInserted(mDocuments.size() - 1);
    }

    public void addAll(Collection<DocumentItem> items) {
        int first = mDocuments.size();
        mDocuments.addAll(items);
        this.notifyItemRangeInserted(first, mDocuments.size() - 1);
    }
}
