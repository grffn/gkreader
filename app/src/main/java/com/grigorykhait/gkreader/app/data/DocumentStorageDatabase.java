/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Grigory Khait
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.grigorykhait.gkreader.app.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

public class DocumentStorageDatabase extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "documents.db";
    public static final int DATABASE_VERSION = 11;
    private static final String LOG_TAG = DocumentStorageDatabase.class.getSimpleName();

    public DocumentStorageDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Tables.DOCUMENTS + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + DocumentContract.DocumentColumns.TITLE + " TEXT NOT NULL,"
                + DocumentContract.DocumentColumns.DESCRIPTION + " TEXT,"
                + DocumentContract.DocumentColumns.FILE_URI + " TEXT NOT NULL,"
                + DocumentContract.DocumentColumns.COVER + " TEXT,"
                + DocumentContract.DocumentColumns.LANG + " TEXT,"
                + DocumentContract.DocumentColumns.LAST_VIEWED + " INTEGER,"
                + DocumentContract.DocumentColumns.LAST_MODIFIED + " INTEGER,"
                + DocumentContract.DocumentColumns.MIME_TYPE + " TEXT, "
                + DocumentContract.DocumentColumns.CURRENT_POSITION + " TEXT, "
                + "UNIQUE (" + DocumentContract.DocumentColumns.FILE_URI + ")" + " ON CONFLICT REPLACE" + ")");

        db.execSQL("CREATE TABLE " + Tables.AUTHORS + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + DocumentContract.AuthorColumns.NAME + " TEXT NOT NULL,"
                + " UNIQUE (" + DocumentContract.AuthorColumns.NAME + ") ON CONFLICT IGNORE" + ")");

        db.execSQL("CREATE TABLE " + Tables.GENRES + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + DocumentContract.GenreColumns.GENRE + " TEXT NOT NULL,"
                + " UNIQUE (" + DocumentContract.GenreColumns.GENRE + ") ON CONFLICT IGNORE" + ")");

        db.execSQL("CREATE TABLE " + Tables.SERIES + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + DocumentContract.SeriesColumns.TITLE + " TEXT NOT NULL,"
                + " UNIQUE (" + DocumentContract.SeriesColumns.TITLE + ") ON CONFLICT IGNORE" + ")");

        db.execSQL("CREATE TABLE " + Tables.DOCUMENTS_AUTHORS + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + DocumentsAuthors.AUTHOR_ID + " INTEGER REFERENCES authors(_id) ON DELETE CASCADE,"
                + DocumentsAuthors.DOCUMENT_ID + " INTEGER  REFERENCES documents(_id) ON DELETE CASCADE,"
                + "UNIQUE (" + DocumentsAuthors.AUTHOR_ID + ", " + DocumentsAuthors.DOCUMENT_ID + ") " +
                "ON CONFLICT REPLACE" + ")");

        db.execSQL("CREATE TABLE " + Tables.DOCUMENTS_GENRES + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + DocumentsGenres.GENRE_ID + " INTEGER REFERENCES authors(_id) ON DELETE CASCADE,"
                + DocumentsGenres.DOCUMENT_ID + " INTEGER  REFERENCES documents(_id) ON DELETE CASCADE,"
                + "UNIQUE (" + DocumentsGenres.GENRE_ID + ", " + DocumentsGenres.DOCUMENT_ID + ") " +
                "ON CONFLICT REPLACE" + ")");

        db.execSQL("CREATE TABLE " + Tables.DOCUMENTS_SERIES + " ("
                + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + DocumentsSeries.SERIES_ID + " INTEGER REFERENCES authors(_id) ON DELETE CASCADE,"
                + DocumentsSeries.DOCUMENT_ID + " INTEGER  REFERENCES documents(_id) ON DELETE CASCADE,"
                + DocumentsSeries.ORDER + " INTEGER, "
                + "UNIQUE (" + DocumentsSeries.SERIES_ID + ", " + DocumentsSeries.DOCUMENT_ID + ")" +
                " ON CONFLICT REPLACE" + ")");

        db.execSQL("create trigger delete_items" +
                " after update of file_uri on documents" +
                " begin delete from documents_authors where documents_authors.document_id = NEW._id;" +
                " delete from documents_genres where documents_genres.document_id = NEW._id;" +
                " delete from documents_series where documents_series.document_id = NEW._id; end");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(LOG_TAG,
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS document");
        db.execSQL("DROP TABLE IF EXISTS author");
        db.execSQL("DROP TABLE IF EXISTS genre");
        db.execSQL("DROP TABLE IF EXISTS " + Tables.DOCUMENTS);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.AUTHORS);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.SERIES);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.GENRES);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.DOCUMENTS_GENRES);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.DOCUMENTS_SERIES);
        db.execSQL("DROP TABLE IF EXISTS " + Tables.DOCUMENTS_AUTHORS);
        db.execSQL("DROP TRIGGER IF EXISTS delete_items");
        onCreate(db);
    }

    interface Tables {
        String DOCUMENTS = "documents";
        String AUTHORS = "authors";
        String SERIES = "series";
        String GENRES = "genres";
        String DOCUMENTS_SERIES = "documents_series";
        String DOCUMENTS_GENRES = "documents_genres";
        String DOCUMENTS_AUTHORS = "documents_authors";

        String DOCUMENTS_SERIES_JOIN_DOCUMENTS = "documents_series " +
                "join documents on documents_series.document_id=documents._id";
        String DOCUMENTS_GENRES_JOIN_DOCUMENTS = "documents_genres " +
                "join documents on documents_genres.document_id=documents._id";
        String DOCUMENTS_AUTHORS_JOIN_DOCUMENTS = "documents_series " +
                "join documents on documents_authors.document_id=documents._id";

        String DOCUMENTS_SERIES_JOIN_SERIES = "documents_series " +
                "join series on documents_series.series_id=series._id";
        String DOCUMENTS_GENRES_JOIN_GENRES = "documents_genres " +
                "join genres on documents_genres.genre_id=genres._id";
        String DOCUMENTS_AUTHORS_JOIN_AUTHORS = "documents_series " +
                "join authors on documents_authors.author_id=authors._id";

        String DOCUMENTS_FULL = "documents" +
                " LEFT JOIN documents_authors ON documents._id=documents_authors.document_id" +
                " LEFT JOIN authors ON authors._id=documents_authors.author_id" +
                " LEFT JOIN documents_genres ON documents._id=documents_genres.genre_id" +
                " LEFT JOIN genres ON genres._id=documents_genres.genre_id" +
                " LEFT JOIN documents_series ON documents._id=documents_series.series_id" +
                " LEFT JOIN series ON series._id=documents_series.series_id";
    }

    public static interface DocumentsSeries {
        String DOCUMENT_ID = "document_id";
        String SERIES_ID = "series_id";
        String ORDER = "ordering";
        Uri CONTENT_URI = DocumentContract.BASE_CONTENT_URI.buildUpon().appendPath(Tables.DOCUMENTS_AUTHORS).build();
    }


    public static interface DocumentsGenres {
        String DOCUMENT_ID = "document_id";
        String GENRE_ID = "genre_id";
        Uri CONTENT_URI = DocumentContract.BASE_CONTENT_URI.buildUpon().appendPath(Tables.DOCUMENTS_AUTHORS).build();
    }

    public static interface DocumentsAuthors {
        String DOCUMENT_ID = "document_id";
        String AUTHOR_ID = "author_id";
        Uri CONTENT_URI = DocumentContract.BASE_CONTENT_URI.buildUpon().appendPath(Tables.DOCUMENTS_AUTHORS).build();


    }
}
