/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Grigory Khait
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.grigorykhait.gkreader.app.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.UUID;

/**
 * Created by Григорий on 14.12.2014.
 */
public class Utils {
    private static final SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final String LOG_TAG = Utils.class.getSimpleName();

    public static String getMimeType(Context context, String fileUri) {
        String extension = MimeTypeMap.getFileExtensionFromUrl(fileUri);
        if (extension != null) {
            return MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return "";
    }

    public static String getMimeType(Context context, Uri fileUri) {
        return getMimeType(context, fileUri.toString());
    }

    public static boolean isAllowed(File file) {
        String filename = file.getName();
        return filename.endsWith(".fb2") || filename.endsWith(".fb2.zip") || filename.endsWith(".txt") || filename.endsWith(".epub");
    }

    public static String saveImage(Context context, Bitmap image) {
        String filename = UUID.randomUUID().toString() + ".png";
        return saveImage(context, image, filename);
    }

    public static String saveImage(Context context, Bitmap image, String filePath) {
        try {
            Log.v(LOG_TAG, filePath);
            FileOutputStream out = context.openFileOutput(filePath, Context.MODE_PRIVATE);
            Log.v(LOG_TAG, out.toString());
            image.compress(Bitmap.CompressFormat.PNG, 0, out);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return context.getFilesDir() + "/" + filePath;
    }

    public static void deleteImage(Context context, String imagePath) {
        Log.v(LOG_TAG, "deleting " + imagePath);
        context.deleteFile(imagePath);
    }

    public static Bitmap getImage(String imagePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        return BitmapFactory.decodeFile(imagePath, options);
    }
}
