package com.grigorykhait.gkreader.app.services;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.grigorykhait.gkreader.logic.DocumentInvalidException;
import com.grigorykhait.gkreader.logic.documents.Document;
import com.grigorykhait.gkreader.logic.documents.EpubDocument;
import com.grigorykhait.gkreader.logic.documents.FictionBookDocument;
import com.grigorykhait.gkreader.logic.documents.PlainTextDocument;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class ParserService extends Service {

    private static final String LOG_TAG = ParserService.class.getSimpleName();
    ParserBinder mBinder = new ParserBinder();
    ParserServiceListener mListener;

    Document mDocument;

    public ParserService() {
        super();
        Log.v(LOG_TAG, "constructor");
    }

    private String getBookCacheName(String fileName) {
        return String.valueOf(fileName.hashCode());
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.v(LOG_TAG, "onBind");
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.v(LOG_TAG, "onCreate");
    }

    public void setListener(ParserServiceListener listener) {
        mListener = listener;
    }

    public void parseDocument(String documentPath) {
        if (mDocument != null && documentPath.equals(mDocument.getFile().getPath())) {
            if (mListener != null) {
                mListener.parsingComplete(mDocument);
            }
        }
        mDocument = null;
        new ParserAsyncTask().execute(documentPath);
    }

    public void prepareSection(String sectionId) {
        new SectionPreparerAsyncTask().execute(sectionId);
    }

    public void prepareFull() {
        new FullPreparerAsyncTask().execute();
    }

    public interface ParserServiceListener {

        void parsingComplete(Document document);

        void getSection(String sectionFilePath);

        void getFull(String url);
    }

    private class ParserAsyncTask extends AsyncTask<String, Void, Document> {

        @Override
        protected Document doInBackground(String... params) {
            try {
                String filePath = params[0];
                Document document = null;
                if (filePath.endsWith(".txt")) {
                    Log.v(LOG_TAG, "txt");
                    document = new PlainTextDocument(filePath);
                } else if (filePath.endsWith(".fb2") || filePath.endsWith(".fb2.zip")) {
                    Log.v(LOG_TAG, "fb2");
                    document = new FictionBookDocument(filePath);
                } else if (filePath.endsWith(".epub")) {
                    Log.v(LOG_TAG, "epub");
                    document = new EpubDocument(filePath);
                }
                if (document != null) {
                    document.parse();
                    return document;
                }

            } catch (IOException | DocumentInvalidException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Document document) {
            super.onPostExecute(document);
            Log.v(LOG_TAG, "onPost Parse document");
            if (mListener != null) {
                mListener.parsingComplete(document);
            }
            mDocument = document;
        }
    }

    private class FullPreparerAsyncTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            if (mDocument == null)
                return null;
            try {
                File cacheDir = getCacheDir();
                File bookCacheDir = new File(cacheDir, ParserService.this.getBookCacheName(mDocument.getFile().getName()));
                if (bookCacheDir.isDirectory() && new File(bookCacheDir, "cache.html").isFile()) {
                    return new File(bookCacheDir, "cache.html").getAbsolutePath();
                }
                if (bookCacheDir.exists()) {
                    bookCacheDir.delete();
                }
                bookCacheDir.mkdir();
                String fileName = "cache.html";
                File cacheFile = new File(bookCacheDir, fileName);
                cacheFile.createNewFile();
                mDocument.writeToFile(cacheFile);
                return cacheFile.getAbsolutePath();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String url) {
            super.onPostExecute(url);
            Log.v(LOG_TAG, "onPost Prepare section");
            if (mListener != null) {
                mListener.getFull(url);
            }
        }
    }

    private class SectionPreparerAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            if (mDocument == null)
                return null;
            try {
                String sectionId = params[0];
                String section = mDocument.getSection(sectionId);
                File cacheDir = getCacheDir();
                String fileName = mDocument.getFile().getName() + "_" + sectionId;
                File cacheFile = new File(cacheDir, fileName);
                DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(cacheFile));
                dataOutputStream.writeUTF(section);
                dataOutputStream.close();
                return cacheFile.getAbsolutePath();
            } catch (DocumentInvalidException | IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String url) {
            super.onPostExecute(url);
            Log.v(LOG_TAG, "onPost Prepare section");
            if (mListener != null) {
                mListener.getSection(url);
            }
        }
    }

    public class ParserBinder extends Binder {

        public ParserService getService() {
            return ParserService.this;
        }
    }
}
