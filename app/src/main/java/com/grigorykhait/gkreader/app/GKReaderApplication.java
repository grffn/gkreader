package com.grigorykhait.gkreader.app;

import android.app.Application;
import android.webkit.WebView;

public class GKReaderApplication extends Application {

    private WebView mWebView;
    private boolean isWebViewInitialized;

    @Override
    public void onCreate() {
        super.onCreate();
        mWebView = new WebView(this);
        isWebViewInitialized = false;
    }

    public WebView getWebView() {
        return mWebView;
    }

    public boolean isWebViewInitialized() {
        return isWebViewInitialized;
    }

    public void setWebViewInitialized() {
        isWebViewInitialized = true;
    }

}
