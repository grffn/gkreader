/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Grigory Khait
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.grigorykhait.gkreader.app.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

import static com.grigorykhait.gkreader.app.data.DocumentContract.Author;
import static com.grigorykhait.gkreader.app.data.DocumentContract.CONTENT_AUTHORITY;
import static com.grigorykhait.gkreader.app.data.DocumentContract.Document;
import static com.grigorykhait.gkreader.app.data.DocumentContract.Genre;
import static com.grigorykhait.gkreader.app.data.DocumentContract.Series;
import static com.grigorykhait.gkreader.app.data.DocumentStorageDatabase.DocumentsAuthors;
import static com.grigorykhait.gkreader.app.data.DocumentStorageDatabase.DocumentsGenres;
import static com.grigorykhait.gkreader.app.data.DocumentStorageDatabase.DocumentsSeries;
import static com.grigorykhait.gkreader.app.data.DocumentStorageDatabase.Tables;

public class DocumentContentProvider extends ContentProvider {
    private static final String LOG_TAG = DocumentContentProvider.class.getSimpleName();
    private static final UriMatcher sUriMatcher = buildUriMatcher();

    private static final int DOCUMENTS = 100;
    private static final int DOCUMENTS_ID = 101;
    private static final int DOCUMENTS_ID_GENRES = 102;
    private static final int DOCUMENTS_ID_SERIES = 103;
    private static final int DOCUMENTS_ID_AUTHORS = 104;
    private static final int DOCUMENTS_FULL = 105;

    private static final int GENRES = 200;
    private static final int GENRES_ID = 201;
    private static final int GENRES_ID_DOCUMENTS = 202;
    private static final int GENRES_NAME = 203;

    private static final int SERIES = 300;
    private static final int SERIES_ID = 301;
    private static final int SERIES_ID_DOCUMENTS = 302;

    private static final int AUTHORS = 400;
    private static final int AUTHORS_ID = 401;
    private static final int AUTHORS_ID_DOCUMENTS = 402;
    private static final int AUTHORS_NAME = 403;

    private static final int DOCUMENTS_GENRES = 1200;
    private static final int DOCUMENTS_SERIES = 1300;
    private static final int DOCUMENTS_AUTHORS = 1400;

    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = CONTENT_AUTHORITY;
        matcher.addURI(authority, "documents", DOCUMENTS);
        matcher.addURI(authority, "documents/#", DOCUMENTS_ID);
        matcher.addURI(authority, "documents/#/genres", DOCUMENTS_ID_GENRES);
        matcher.addURI(authority, "documents/#/series", DOCUMENTS_ID_SERIES);
        matcher.addURI(authority, "documents/#/authors", DOCUMENTS_ID_AUTHORS);
        matcher.addURI(authority, "documents/full", DOCUMENTS_FULL);

        matcher.addURI(authority, "genres", GENRES);
        matcher.addURI(authority, "genres/#", GENRES_ID);
        matcher.addURI(authority, "genres/#/documents", GENRES_ID_DOCUMENTS);
        matcher.addURI(authority, "genres/*", GENRES_NAME);

        matcher.addURI(authority, "series", SERIES);
        matcher.addURI(authority, "series/#", SERIES_ID);
        matcher.addURI(authority, "series/#/documents", SERIES_ID_DOCUMENTS);

        matcher.addURI(authority, "authors", AUTHORS);
        matcher.addURI(authority, "authors/#", AUTHORS_ID);
        matcher.addURI(authority, "authors/#/documents", AUTHORS_ID_DOCUMENTS);
        matcher.addURI(authority, "authors/*", AUTHORS_NAME);

        matcher.addURI(authority, "documents_genres", DOCUMENTS_GENRES);
        matcher.addURI(authority, "documents_series", DOCUMENTS_SERIES);
        matcher.addURI(authority, "documents_authors", DOCUMENTS_AUTHORS);

        return matcher;
    }

    private DocumentStorageDatabase mOpenHelper;

    public DocumentContentProvider() {
    }

    @Override
    public boolean onCreate() {
        mOpenHelper = new DocumentStorageDatabase(getContext());
        return true;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {

        Log.v(LOG_TAG, "delete " + uri);
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsDeleted;

        switch (match) {
            case DOCUMENTS: {
                rowsDeleted = db.delete(Tables.DOCUMENTS, selection, selectionArgs);
                break;
            }
            case GENRES: {
                rowsDeleted = db.delete(Tables.GENRES, selection, selectionArgs);
                break;
            }
            case AUTHORS: {
                rowsDeleted = db.delete(Tables.AUTHORS, selection, selectionArgs);
                break;
            }
            case SERIES: {
                rowsDeleted = db.delete(Tables.SERIES, selection, selectionArgs);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (selection == null || rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    @Override
    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case DOCUMENTS:
                return Document.CONTENT_TYPE;
            case DOCUMENTS_ID:
                return Document.CONTENT_ITEM_TYPE;
            case DOCUMENTS_ID_GENRES:
                return Genre.CONTENT_TYPE;
            case DOCUMENTS_ID_SERIES:
                return Series.CONTENT_TYPE;
            case DOCUMENTS_ID_AUTHORS:
                return Author.CONTENT_TYPE;
            case DOCUMENTS_FULL:
                return Document.CONTENT_TYPE;

            case GENRES:
                return Genre.CONTENT_TYPE;
            case GENRES_ID:
                return Genre.CONTENT_ITEM_TYPE;
            case GENRES_ID_DOCUMENTS:
                return Document.CONTENT_TYPE;

            case AUTHORS:
                return Author.CONTENT_TYPE;
            case AUTHORS_ID:
                return Author.CONTENT_ITEM_TYPE;
            case AUTHORS_ID_DOCUMENTS:
                return Document.CONTENT_TYPE;
            case AUTHORS_NAME:
                return Author.CONTENT_ITEM_TYPE;

            case SERIES:
                return Series.CONTENT_TYPE;
            case SERIES_ID:
                return Series.CONTENT_ITEM_TYPE;
            case SERIES_ID_DOCUMENTS:
                return Document.CONTENT_TYPE;

            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Log.v(LOG_TAG, "insert " + uri);
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri retUri;

        switch (match) {
            case DOCUMENTS: {
                long _id = db.insert(Tables.DOCUMENTS, null, values);
                if (_id > 0)
                    retUri = Document.buildDocumentUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case GENRES: {
                long _id = db.insertWithOnConflict(Tables.GENRES, null, values, SQLiteDatabase.CONFLICT_IGNORE);
                if (_id > 0)
                    retUri = Genre.buildGenreUri(_id);

                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case AUTHORS: {
                long _id = db.insertWithOnConflict(Tables.AUTHORS, null, values, SQLiteDatabase.CONFLICT_IGNORE);
                if (_id > 0)
                    retUri = Author.buildAuthorUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case SERIES: {
                long _id = db.insertWithOnConflict(Tables.SERIES, null, values, SQLiteDatabase.CONFLICT_IGNORE);
                if (_id > 0)
                    retUri = Series.buildSeriesUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case DOCUMENTS_AUTHORS: {
                long _id = db.insertWithOnConflict(Tables.DOCUMENTS_AUTHORS, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                if (_id > 0)
                    retUri = ContentUris.withAppendedId(DocumentsAuthors.CONTENT_URI, _id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case DOCUMENTS_SERIES: {
                long _id = db.insertWithOnConflict(Tables.DOCUMENTS_SERIES, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                if (_id > 0)
                    retUri = ContentUris.withAppendedId(DocumentsSeries.CONTENT_URI, _id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case DOCUMENTS_GENRES: {
                long _id = db.insertWithOnConflict(Tables.DOCUMENTS_GENRES, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                if (_id > 0)
                    retUri = ContentUris.withAppendedId(DocumentsGenres.CONTENT_URI, _id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return retUri;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        Log.v(LOG_TAG, "query " + uri);
        Cursor retCursor;
        int match = sUriMatcher.match(uri);
        switch (match) {
            case DOCUMENTS:
                retCursor = getTable(Tables.DOCUMENTS,
                        projection, selection, selectionArgs, sortOrder);
                break;
            case DOCUMENTS_FULL:
                retCursor = mOpenHelper.getReadableDatabase().query(Tables.DOCUMENTS_FULL,
                        DOCUMENTS_FULL_PROJECTION, selection, selectionArgs,
                        "documents._id", null, sortOrder);
                break;
            case DOCUMENTS_ID:
                retCursor = getItemById(Tables.DOCUMENTS,
                        projection, ContentUris.parseId(uri));
                break;
            case DOCUMENTS_ID_GENRES:
                retCursor = getItemById(Tables.DOCUMENTS_GENRES_JOIN_GENRES, projection,
                        Document.getDocumentId(uri));
                break;
            case DOCUMENTS_ID_SERIES:
                retCursor = getItemById(Tables.DOCUMENTS_SERIES_JOIN_SERIES, projection,
                        Document.getDocumentId(uri));
                break;
            case DOCUMENTS_ID_AUTHORS:
                retCursor = getItemById(Tables.DOCUMENTS_AUTHORS_JOIN_AUTHORS, projection,
                        Document.getDocumentId(uri));
                break;

            case SERIES:
                retCursor = getTable(Tables.SERIES,
                        projection, selection, selectionArgs, sortOrder);
                break;
            case SERIES_ID:
                retCursor = getItemById(Tables.SERIES,
                        projection, ContentUris.parseId(uri));
                break;
            case SERIES_ID_DOCUMENTS:
                retCursor = getItemById(Tables.DOCUMENTS_SERIES_JOIN_DOCUMENTS, projection,
                        Document.getDocumentId(uri));
                break;

            case GENRES:
                retCursor = getTable(Tables.GENRES,
                        projection, selection, selectionArgs, sortOrder);
                break;
            case GENRES_ID:
                retCursor = getItemById(Tables.GENRES,
                        projection, ContentUris.parseId(uri));
                break;
            case GENRES_ID_DOCUMENTS:
                retCursor = getItemById(Tables.DOCUMENTS_GENRES_JOIN_DOCUMENTS, projection,
                        Document.getDocumentId(uri));
                break;
            case GENRES_NAME:
                String name = uri.getPathSegments().get(1);
                retCursor = getTable(Tables.GENRES,
                        projection, DocumentContract.GenreColumns.GENRE + "=?", new String[]{name},
                        null);
                break;
            case AUTHORS:
                retCursor = getTable(Tables.AUTHORS,
                        projection, selection, selectionArgs, sortOrder);
                break;
            case AUTHORS_ID:
                retCursor = getItemById(Tables.AUTHORS,
                        projection, ContentUris.parseId(uri));
                break;
            case AUTHORS_ID_DOCUMENTS:
                retCursor = getItemById(Tables.DOCUMENTS_AUTHORS_JOIN_DOCUMENTS, projection,
                        Document.getDocumentId(uri));
                break;
            case AUTHORS_NAME:
                String authorName = uri.getPathSegments().get(1);
                retCursor = getTable(Tables.AUTHORS,
                        projection, DocumentContract.AuthorColumns.NAME + "=?", new String[]{authorName}, null);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        Log.v(LOG_TAG, "bulk insert " + uri);
        final int match = sUriMatcher.match(uri);
        int rowsInserted;
        switch (match) {
            case DOCUMENTS:
                rowsInserted = bulkInsert(Tables.DOCUMENTS, values, uri);
                break;
            case GENRES:
                rowsInserted = bulkInsert(Tables.GENRES, values, uri);
                break;
            case DOCUMENTS_SERIES:
                rowsInserted = bulkInsert(Tables.DOCUMENTS_SERIES, values, uri);
                break;
            case DOCUMENTS_AUTHORS:
                rowsInserted = bulkInsert(Tables.DOCUMENTS_AUTHORS, values, uri);
                break;
            case DOCUMENTS_GENRES:
                rowsInserted = bulkInsert(Tables.DOCUMENTS_GENRES, values, uri);
                break;
            default:
                rowsInserted = super.bulkInsert(uri, values);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsInserted;
    }

    private int bulkInsert(String table, ContentValues[] values, Uri uri) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        db.beginTransaction();
        int returnCount = 0;
        try {
            for (ContentValues value : values) {
                long _id = db.insert(table, null, value);
                if (_id != -1) {
                    returnCount++;
                }
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        return returnCount;
    }

    private Cursor getTable(String table, String[] projection, String selection,
                            String[] selectionArgs, String sortOrder) {
        return mOpenHelper.getReadableDatabase().query(
                table,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder);
    }

    private Cursor getItemById(String table, String[] projection, long id) {
        return mOpenHelper.getReadableDatabase().query(
                table,
                projection,
                BaseColumns._ID + "=?",
                new String[]{String.valueOf(id)},
                null,
                null,
                null);
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsUpdated;

        switch (match) {
            case DOCUMENTS: {
                rowsUpdated = db.update(Tables.DOCUMENTS, values, selection, selectionArgs);
                break;
            }
            case GENRES: {
                rowsUpdated = db.update(Tables.GENRES, values, selection, selectionArgs);
                break;
            }
            case AUTHORS: {
                rowsUpdated = db.update(Tables.AUTHORS, values, selection, selectionArgs);
                break;
            }
            case SERIES: {
                rowsUpdated = db.update(Tables.SERIES, values, selection, selectionArgs);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }

    private static final String[] DOCUMENTS_FULL_PROJECTION = new String[]{
            DocumentContract.DocumentsFullQuery._ID,
            DocumentContract.DocumentsFullQuery.TITLE,
            DocumentContract.DocumentsFullQuery.DESCRIPTION,
            DocumentContract.DocumentsFullQuery.FILE_URI,
            DocumentContract.DocumentsFullQuery.COVER,
            DocumentContract.DocumentsFullQuery.LANG,
            " GROUP_CONCAT('{' || authors._id || ',' || authors.name || '}')" +
                    " AS " + DocumentContract.DocumentsFullQuery.AUTHORS,
            " GROUP_CONCAT('{' || genres._id || ',' || genres.name || '}')" +
                    " AS " + DocumentContract.DocumentsFullQuery.GENRES,
            " GROUP_CONCAT('{' || series._id || ',' || series.title || '}')" +
                    " AS " + DocumentContract.DocumentsFullQuery.SERIES};
}
