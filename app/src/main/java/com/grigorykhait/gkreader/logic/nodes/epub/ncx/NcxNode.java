package com.grigorykhait.gkreader.logic.nodes.epub.ncx;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "ncx", strict = false)
public class NcxNode {

    @Path("head/")
    @ElementList(entry = "meta", inline = true)
    private List<MetaNode> metas;

    @Element(name = "text")
    @Path("docTitle")
    private String title;

    @ElementList(entry = "text", required = false, inline = true)
    @Path("docAuthor/")
    private List<String> authors;

    @Element
    private NavMapNode navMap;

    @Element(required = false)
    private PageListNode pageList;

    @ElementList(required = false, inline = true, entry = "navList")
    private List<NavListNode> navList;

    public List<MetaNode> getMetas() {
        return metas;
    }

    public String getTitle() {
        return title;
    }

    public NavMapNode getNavMap() {
        return navMap;
    }

    public List<NavListNode> getNavList() {
        return navList;
    }

    public PageListNode getPageList() {
        return pageList;
    }

    public List<String> getAuthors() {
        return authors;
    }

}
