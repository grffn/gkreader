package com.grigorykhait.gkreader.logic.nodes.epub.ncx;

import org.simpleframework.xml.ElementList;

import java.util.List;

public class NavMapNode {

    @ElementList(entry = "navPoint", inline = true)
    private List<NavPointNode> navPoints;

    public List<NavPointNode> getNavPoints() {
        return navPoints;
    }
}
