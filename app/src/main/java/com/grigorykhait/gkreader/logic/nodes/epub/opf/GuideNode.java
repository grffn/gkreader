package com.grigorykhait.gkreader.logic.nodes.epub.opf;

import org.simpleframework.xml.Attribute;

public class GuideNode {

    public static final String REFERENCE_TYPE_COVER = "cover";
    public static final String REFERENCE_TYPE_INDEX = "index";
    public static final String REFERENCE_TYPE_TEXT = "text";
    public static final String REFERENCE_TYPE_PREFACE = "preface";
    public static final String REFERENCE_TYPE_NOTES = "notes";
    public static final String REFERENCE_TYPE_EPIGRAPH = "epigraph";
    public static final String REFERENCE_TYPE_GLOSSARY = "glossary";
    public static final String REFERENCE_TYPE_TOC = "toc";
    public static final String REFERENCE_TYPE_LOI = "loi";
    public static final String REFERENCE_TYPE_LOT = "lot";
    public static final String REFERENCE_TYPE_TITLE_PAGE = "title-page";
    public static final String REFERENCE_TYPE_BIBLIOGRAPHY = "bibliography";

    public static class ReferenceNode {


        @Attribute
        private String type;

        @Attribute(required = false)
        private String title;

        @Attribute
        private String href;
    }
}
