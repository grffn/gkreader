package com.grigorykhait.gkreader.logic.nodes.fb3.descr;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.List;

public class SequenceNode {
    @Element
    public TitleNode title;

    @Element(required = false)
    public SequenceNode sequence;

    @Attribute(required = false)
    public int number;

    @Attribute
    public String id;
}
