package com.grigorykhait.gkreader.logic.nodes.epub.ocf;

import org.simpleframework.xml.Attribute;

public class LinkNode {

    @Attribute

    private String href;
    @Attribute
    private String rel;
    @Attribute(name = "media-type", required = false)
    private String mediaType;

    public String getHref() {
        return href;
    }

    public String getRel() {
        return rel;
    }

    public String getMediaType() {
        return mediaType;
    }
}
