/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Grigory Khait
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.grigorykhait.gkreader.logic.nodes.fb2.content;

import org.simpleframework.xml.convert.Converter;
import org.simpleframework.xml.stream.InputNode;
import org.simpleframework.xml.stream.OutputNode;

import java.io.IOException;
import java.io.PrintStream;

public class PNode extends ContentNode {

    private final static String EMPHASIS = "em";
    private final static String STRONG = "strong";
    private final static String SUB = "sub";
    private final static String SUP = "sup";
    private final static String STRIKE = "s";
    private final static String CODE = "code";
    private final static String LINK = "a";

    private String text;

    private String style;


    private boolean subtitle;

    public boolean isSubtitle() {
        return subtitle;
    }

    @Override
    public String getRepresentation() {
        if (text == null) {
            return "";
        }
        return text;
    }

    @Override
    public void write(PrintStream out) throws IOException {
        if (text == null) {
            out.print("");
        }
        out.print(text);
    }

    public static class PNodeBuilder {

        private final static String TAG = "<%s>%s</%s>";

        private StringBuilder pNodeBuilder = new StringBuilder("<p>");
        private String style;

        private boolean subtitle = false;

        public PNodeBuilder addText(String text) {
            pNodeBuilder.append(text == null ? "" : text);
            return this;
        }

        private PNodeBuilder addTaggedText(String tag, String text) {
            pNodeBuilder.append(String.format(TAG, tag, text, tag));
            return this;
        }

        public PNodeBuilder addLink(String text, String href, String type) {
            addText(String.format("<a %s %s>%s</a>",
                    href.isEmpty() ? "" : "href=\'" + href + "\'",
                    type.isEmpty() ? "" : "type=\'" + type + "\'",
                    text));
            return this;
        }

        public PNodeBuilder setStyle(String styleText) {
            style = styleText;
            return this;
        }

        public PNodeBuilder setSubtitle(boolean subtitle) {
            this.subtitle = subtitle;
            return this;
        }

        public PNode build() {
            PNode node = new PNode();
            pNodeBuilder.append("</p>");
            node.text = pNodeBuilder.toString();
            node.style = style;
            node.subtitle = subtitle;
            return node;
        }
    }

    public static class PNodeConverter implements Converter<PNode> {

        @Override
        public PNode read(InputNode node) throws Exception {
            PNodeBuilder builder = new PNodeBuilder();
            String tag = node.getName();
            builder.setSubtitle(tag.equalsIgnoreCase("subtitle"));
            builder.addText(node.getValue());
            InputNode inputNode = node.getNext();
            while (inputNode != null) {
                if (inputNode.getName().equalsIgnoreCase("emphasis")) {
                    builder.addTaggedText(EMPHASIS, inputNode.getValue());
                } else if (inputNode.getName().equalsIgnoreCase("sub")) {
                    builder.addTaggedText(SUB, inputNode.getValue());
                } else if (inputNode.getName().equalsIgnoreCase("sup")) {
                    builder.addTaggedText(SUP, inputNode.getValue());
                } else if (inputNode.getName().equalsIgnoreCase("strong")) {
                    builder.addTaggedText(STRONG, inputNode.getValue());
                } else if (inputNode.getName().equalsIgnoreCase("code")) {
                    builder.addTaggedText(CODE, inputNode.getValue());
                } else if (inputNode.getName().equalsIgnoreCase("strike")) {
                    builder.addTaggedText(STRIKE, inputNode.getValue());
                } else if (inputNode.getName().equalsIgnoreCase("a")) {
                    InputNode hrefAttr = inputNode.getAttribute("href");
                    InputNode typeAttr = inputNode.getAttribute("type");
                    String href = "", type = "";
                    if (hrefAttr != null) {
                        href = hrefAttr.getValue();
                    }
                    if (typeAttr != null) {
                        type = typeAttr.getValue();
                    }
                    builder.addLink(inputNode.getValue(), href, type);
                }
                builder.addText(node.getValue());
                inputNode = node.getNext();
            }
            return builder.build();
        }

        @Override
        public void write(OutputNode node, PNode value) throws Exception {

        }

    }
}
