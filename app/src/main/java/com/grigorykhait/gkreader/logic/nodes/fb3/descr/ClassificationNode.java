package com.grigorykhait.gkreader.logic.nodes.fb3.descr;

import org.simpleframework.xml.ElementList;

import java.util.List;

public class ClassificationNode {

    @ElementList(inline = true, entry = "subject")
    public List<String> subjects;
}
