package com.grigorykhait.gkreader.logic.nodes.epub.ncx;

import org.simpleframework.xml.Attribute;

public class NavTargetNode {

    @Attribute
    private String id;

    @Attribute(required = false, name = "class")
    private String classText;

    @Attribute(required = false)
    private String value;

    @Attribute(required = false)
    private String playOrder;
}
