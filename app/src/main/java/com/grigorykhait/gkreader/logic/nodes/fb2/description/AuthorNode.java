/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Grigory Khait
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.grigorykhait.gkreader.logic.nodes.fb2.description;

import com.grigorykhait.gkreader.logic.nodes.fb2.Fb2Node;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.List;

/**
 * Created by grffn on 19.03.2015.
 */
public class AuthorNode extends Fb2Node {

    @Element(name = "first-name", required = false)
    String firstName;

    @Element(name = "middle-name", required = false)
    String middleName;

    @Element(name = "last-name", required = false)
    String lastName;

    @Element(name = "nickname", required = false)
    String nickname;

    @ElementList(entry = "homepage", inline = true, required = false)
    List<String> homepages;

    @ElementList(entry = "email", inline = true, required = false)
    List<String> emails;

    @Element(name = "id", required = false)
    String id;

    @Override
    public String getRepresentation() {
        return null;
    }


    public String getAuthorName() {
        StringBuilder nameBuilder = new StringBuilder();
        if (firstName != null) {
            nameBuilder.append(firstName + " ");
            if (middleName != null) {
                nameBuilder.append(middleName + " ");
            }
            nameBuilder.append(lastName);
        } else {
            nameBuilder.append(nickname);
        }
        return nameBuilder.toString();
    }
}
