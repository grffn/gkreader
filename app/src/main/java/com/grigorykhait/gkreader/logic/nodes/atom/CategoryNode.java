package com.grigorykhait.gkreader.logic.nodes.atom;

import org.simpleframework.xml.Element;

public class CategoryNode {
    @Element
    private String term;

    @Element(required = false)
    private String scheme;

    @Element(required = false)
    private String label;
}
