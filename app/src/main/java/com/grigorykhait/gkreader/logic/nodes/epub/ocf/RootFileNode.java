package com.grigorykhait.gkreader.logic.nodes.epub.ocf;

import org.simpleframework.xml.Attribute;

public class RootFileNode {

    @Attribute(name = "full-path")
    private String fullPath;


    public String getFullPath() {
        return fullPath;
    }
}
