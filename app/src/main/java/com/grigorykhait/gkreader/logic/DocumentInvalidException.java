package com.grigorykhait.gkreader.logic;

public class DocumentInvalidException extends Exception {

    public static final String DETAIL_MESSAGE = "Document is invalid";

    public DocumentInvalidException() {
        super(DETAIL_MESSAGE);
    }

    public DocumentInvalidException(Throwable inner) {
        super(DETAIL_MESSAGE, inner);
    }
}
