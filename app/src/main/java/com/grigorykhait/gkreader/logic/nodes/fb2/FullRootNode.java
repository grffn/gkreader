/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Grigory Khait
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.grigorykhait.gkreader.logic.nodes.fb2;

import com.grigorykhait.gkreader.logic.nodes.fb2.description.DescriptionNode;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.IOException;
import java.io.PrintStream;
import java.util.List;

@Root(name = "FictionBook")
public class FullRootNode extends RootNode {

    @ElementList(inline = true, entry = "body")
    private List<BodyNode> bodies;
    @Element
    private DescriptionNode description;
    @ElementList(inline = true, required = false, entry = "binary")
    private List<BinaryNode> images;

    public FullRootNode() {
    }

    @Override
    public DescriptionNode getDescription() {
        return description;
    }

    @Override
    public String getRepresentation() {
        StringBuilder builder = new StringBuilder();
        builder.append(getBody().getRepresentation());
        if (getNotes() != null) {
            builder.append(getNotes().getRepresentation());
        }
        return builder.toString();
    }

    public void write(PrintStream out) throws IOException {
        if (out == null){
            throw new IllegalArgumentException();
        }
        getBody().write(out);
        if (getNotes()!=null){
            getNotes().write(out);
        }
    }

    @Override
    public BodyNode getBody() {
        return bodies.get(0);
    }

    @Override
    public BodyNode getNotes() {
        return bodies.size() == 2 ? bodies.get(1) : null;
    }

    @Override
    public BinaryNode findImageById(String href) {
        String id = href.charAt(0) == '#' ? href.substring(1) : href;
        if (images == null) {
            return null;
        }
        for (BinaryNode image : images) {
            if (image.getId().equals(id)) {
                return image;
            }
        }
        return null;
    }

    @Override
    public List<BinaryNode> getImages() {
        return images;
    }

}
