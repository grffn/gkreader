package com.grigorykhait.gkreader.logic.nodes.fb3.descr;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.List;

public class SubjectRelationNode {

    @Element
    public TitleNode title;

    @Element(name = "first-name", required = false)
    public String firstName;

    @Element(name = "middle-name", required = false)
    public String middleName;

    @Element(name = "last-name")
    public String lastName;

    @Element(required = false)
    public String description;

    @Attribute
    public String id;

    @Attribute
    public String link;

    public class SubjectRelationEnum {

    }
}
