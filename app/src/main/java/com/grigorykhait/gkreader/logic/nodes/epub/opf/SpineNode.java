package com.grigorykhait.gkreader.logic.nodes.epub.opf;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;

import java.util.List;

public class SpineNode {

    @Attribute(required = false)
    private String id;

    @Attribute(required = false)
    private String toc;

    @Attribute(required = false, name = "page-progression-detection")
    private String pageProgressionDetection;

    @ElementList(entry = "itemref", inline = true)
    private List<ItemRefNode> itemRefs;

    public String getToc() {
        return toc;
    }

    public List<ItemRefNode> getItemRefs() {
        return itemRefs;
    }

    public static class ItemRefNode {

        @Attribute
        public String idref;

        @Attribute(required = false)
        public String linear;

        @Attribute(required = false)
        public String id;

        @Attribute(required = false)
        public String properties;
    }
}
