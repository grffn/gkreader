package com.grigorykhait.gkreader.logic.nodes.epub.ocf;

import com.grigorykhait.gkreader.logic.DocumentInvalidException;
import com.grigorykhait.gkreader.logic.nodes.Node;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "container", strict = false)
public class ContainerNode extends Node {

    @Path("rootfiles")
    @ElementList(entry = "rootfile", inline = true)
    private List<RootFileNode> rootfiles;

    @Path("links")
    @ElementList(name = "links", entry = "link", inline = true, required = false)
    private List<LinkNode> links;

    public List<RootFileNode> getRootfiles() {
        return rootfiles;
    }

    public List<LinkNode> getLinks() {
        return links;
    }

    public String getDefaultRootfile() throws DocumentInvalidException {
        if (rootfiles.size() < 1) {
            throw new DocumentInvalidException();
        }
        return rootfiles.get(0).getFullPath();
    }

    @Override
    public String getRepresentation() {
        return null;
    }
}
