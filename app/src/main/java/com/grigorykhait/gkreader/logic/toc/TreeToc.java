package com.grigorykhait.gkreader.logic.toc;

import java.util.ArrayList;
import java.util.List;

public class TreeToc {

    private Node root;

    public TreeToc(Node root) {
        this.root = root;
    }

    public Node getRoot() {
        return root;
    }

    public static class Node {
        public String getId;
        private Node parent;
        private List<Node> children;
        private String label;
        private String kind;
        private String id;

        public Node(Node parent, String label, String kind, String id) {
            this.parent = parent;
            this.label = label;
            this.kind = kind;
            this.id = id;
            children = new ArrayList<>();
        }

        public void addChild(Node child) {
            children.add(child);
        }
    }


}
