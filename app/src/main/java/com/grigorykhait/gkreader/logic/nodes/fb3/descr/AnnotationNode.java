package com.grigorykhait.gkreader.logic.nodes.fb3.descr;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Text;

import java.util.List;

public class AnnotationNode {

    @Text
    public String content;

}
