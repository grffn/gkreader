package com.grigorykhait.gkreader.logic.nodes.fb3;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "Relationships")
public class RelsNode {

    @ElementList(entry = "Relationship", inline = true)
    private List<RelationshipNode> relationships;

    public class RelationshipNode {

        @Attribute(name = "TargetMode", required = false)
        public String targetMode;

        @Attribute(name = "Target")
        public String target;

        @Attribute(name = "Id")
        public String id;

        @Attribute(name = "Type")
        public String type;

    }
}
