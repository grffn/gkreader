package com.grigorykhait.gkreader.logic.nodes.epub.opf;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;

import java.util.List;
import java.util.NoSuchElementException;

public class ManifestNode {

    @ElementList(inline = true, entry = "item")
    private List<ItemNode> nodes;

    public List<ItemNode> getNodes() {
        return nodes;
    }

    public ItemNode findNodeById(String id) {
        for (ItemNode node : nodes) {
            if (node.id.equals(id)) {
                return node;
            }
        }
        throw new NoSuchElementException("Element " + id + " is not in manifest");
    }


    public static class ItemNode {

        @Attribute
        public String id;

        @Attribute
        public String href;

        @Attribute(name = "media-type")
        public String mediaType;

        @Attribute(required = false)
        public String fallback;

        @Attribute(required = false)
        public String properties;

        @Attribute(name = "media-overlay", required = false)
        public String mediaOverlay;
    }
}
