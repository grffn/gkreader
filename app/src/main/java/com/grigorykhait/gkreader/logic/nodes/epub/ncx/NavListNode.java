package com.grigorykhait.gkreader.logic.nodes.epub.ncx;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;

import java.util.List;

public class NavListNode {

    @Path("navLabel/text")
    @ElementList(required = false)
    private List<String> info;

    @Path("navLabel/text")
    @ElementList
    private List<String> labels;

    @ElementList(entry = "navTarget", inline = true)
    private List<String> navTargets;
}
