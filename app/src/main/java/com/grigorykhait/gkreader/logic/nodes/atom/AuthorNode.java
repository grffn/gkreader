package com.grigorykhait.gkreader.logic.nodes.atom;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

public class AuthorNode {

    @Element(required = true)
    private String name;

    @Element(required = false)
    private String uri;

    @Element(required = false)
    private String email;
}
