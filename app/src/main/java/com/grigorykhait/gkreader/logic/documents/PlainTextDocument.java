/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Grigory Khait
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.grigorykhait.gkreader.logic.documents;

import android.graphics.Bitmap;

import com.grigorykhait.gkreader.logic.DocumentInvalidException;
import com.grigorykhait.gkreader.logic.nodes.Node;
import com.grigorykhait.gkreader.logic.nodes.txt.RootNode;
import com.grigorykhait.gkreader.logic.parsers.PlainTextParser;
import com.grigorykhait.gkreader.logic.toc.TreeToc;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class PlainTextDocument extends Document {

    private PlainTextParser mParser;
    private String mFilePath;
    private String mTitle;
    private Node mRootNode;

    public PlainTextDocument(String filePath) throws IOException {
        super(filePath);
        mParser = new PlainTextParser(new FileReader(filePath));
        mFilePath = filePath;
        mTitle = filePath.substring(filePath.lastIndexOf("/"), filePath.length());

    }

    @Override
    public void parse() {
        try {
            setRootNode(mParser.parse());
        } catch (IOException exc) {
            throw new IllegalArgumentException("File path" + mFilePath + " is not valid");
        }
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    @Override
    public Bitmap getCover() {
        return null;
    }

    @Override
    public String getAnnotation() {
        return null;
    }

    @Override
    public List<String> getAuthors() {
        return null;
    }

    @Override
    public List<String> getGenres() {
        return null;
    }

    @Override
    public String getLanguageCode() {
        return null;
    }

    public RootNode getRootNode() {
        return (RootNode) mRootNode;
    }

    protected void setRootNode(Node root) {
        mRootNode = root;
    }

    @Override
    public String getRepresentation() {
        if (getRootNode() != null) {
            return getRootNode().getRepresentation();
        }
        return "";
    }

    @Override
    public String getSection(String sectionId) throws DocumentInvalidException {
        return null;
    }

    @Override
    public void writeToFile(File file) {
        try {
            PrintStream out = new PrintStream(file);
            out.print(PREPEND);
            getRootNode().write(out);
            out.print(APPEND);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public TreeToc getToc() {
        TreeToc.Node node = new TreeToc.Node(null, getTitle(), "title", "");
        return new TreeToc(node);
    }

    @Override
    public List<Attachment> getAttachments() {
        return new ArrayList<>();
    }
}
