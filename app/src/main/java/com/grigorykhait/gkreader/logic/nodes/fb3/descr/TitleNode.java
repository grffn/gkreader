/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Grigory Khait
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.grigorykhait.gkreader.logic.nodes.fb3.descr;

import com.grigorykhait.gkreader.logic.nodes.fb2.AnnotationNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.DateNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.Fb2Node;
import com.grigorykhait.gkreader.logic.nodes.fb2.content.ImageNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.description.AuthorNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.description.SequenceNode;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;

import java.util.ArrayList;
import java.util.List;

public class TitleNode {

    @Element
    public String main;

    @Element(required = false)
    public String sub;

    @ElementList(required = false, entry = "alt", inline = false)
    public List<String> alt;

}
