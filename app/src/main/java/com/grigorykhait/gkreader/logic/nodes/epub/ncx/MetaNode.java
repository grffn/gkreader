package com.grigorykhait.gkreader.logic.nodes.epub.ncx;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

public class MetaNode {

    @Attribute(name = "name")
    private String name;

    @Attribute(name = "content")
    private String content;

    @Attribute(required = false)
    private String scheme;
}
