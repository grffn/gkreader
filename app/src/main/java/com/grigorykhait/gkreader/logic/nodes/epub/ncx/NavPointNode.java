package com.grigorykhait.gkreader.logic.nodes.epub.ncx;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;

import java.util.List;

public class NavPointNode {

    @Attribute
    private String id;

    @Attribute(name = "class", required = false)
    private String classText;

    @Attribute(required = false)
    private String playOrder;

    @Path("navLabel/")
    @ElementList(entry = "text", inline = true)
    private List<String> labels;

    @Attribute(name = "src", required = false)
    @Path("content")
    private String content;

    @ElementList(inline = true, required = false, entry = "navPoint")
    private List<NavPointNode> navPoints;

    public String getId() {
        return id;
    }

    public String getClassText() {
        return classText;
    }

    public List<String> getLabels() {
        return labels;
    }

    public String getLabel() {
        return labels.get(0);
    }

    public String getPlayOrder() {
        return playOrder;
    }

    public String getContent() {
        return content;
    }

    public List<NavPointNode> getNavPoints() {
        return navPoints;
    }
}
