package com.grigorykhait.gkreader.logic.nodes.fb3.descr;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;

import java.util.List;

public class DescriptionNode {

    @Element(name = "title-info")
    public TitleNode titleInfo;

    @ElementList(entry = "sequence", required = false, inline = true)
    public List<SequenceNode> sequences;

    @Element(name = "fb3-classification")
    public ClassificationNode classification;

    @ElementList(name = "subject")
    @Path("fb3-relations")
    public List<String> subjectRelations;

    @ElementList(name = "object", required = false)
    @Path("fb3-relations")
    public List<String> objectRelations;


    @Element
    public String lang;

    @Element(required = false)
    public AnnotationNode annotation;

    @Attribute
    public String id;

    @Attribute
    public String version;
}
