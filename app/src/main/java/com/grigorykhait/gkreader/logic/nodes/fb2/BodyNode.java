/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Grigory Khait
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.grigorykhait.gkreader.logic.nodes.fb2;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;

/**
 * Created by grffn on 15.03.2015.
 */
public class BodyNode extends Fb2Node {

    @Element(required = false)

    private String title;
    @ElementList(inline = true, entry = "section")
    private List<SectionNode> sections;
    @Attribute(required = false)
    private String lang;

    public String getTitle() {
        if (title == null)
            return "";
        return title;
    }

    public List<SectionNode> getSections() {
        return sections;
    }

    @Override
    public String getRepresentation() {
        StringBuilder builder = new StringBuilder();
        if (title != null && !title.equals(""))
            builder.append("<h1>" + title + "</h1>");
        for (SectionNode section : sections) {
            builder.append(section.getRepresentation());
        }
        return builder.toString();
    }

    public void write(PrintStream out) throws IOException {
        if (title != null && !title.equals(""))
            out.print("<h1>" + title + "</h1>");
        for (SectionNode section : sections) {
            section.write(out);
        }
    }


}
