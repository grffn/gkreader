package com.grigorykhait.gkreader.logic.nodes.atom;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.Date;
import java.util.List;

@Root(name = "entry")
public class EntryNode {
    @Attribute(required = false)
    private String base;
    @Attribute(required = false)
    private String lang;

    @ElementList(entry = "author", required = false, inline = true)
    private List<AuthorNode> authors;

    @ElementList(entry = "category", required = false, inline = true)
    private List<CategoryNode> categories;

    @ElementList(entry = "link", required = false, inline = true)
    private List<LinkNode> links;

    @ElementList(entry = "contributor", required = false, inline = true)
    private List<AuthorNode> contributors;

    @Element
    private String id;

    @Element(required = false)
    private TextConstructNode content;

    @Element(required = false)
    private TextConstructNode rights;

    @Element(required = false)
    private SourceNode source;

    @Element(required = false)
    private Date published;

    @Element(required = false)
    private Date updated;

    @Element
    private TextConstructNode title;

    @Element(required = false)
    private TextConstructNode subtitle;

    @Element(required = false)
    private TextConstructNode summary;

    @Element(required = false)
    private String logo;
}
