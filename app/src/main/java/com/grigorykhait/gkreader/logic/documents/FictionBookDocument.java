/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Grigory Khait
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.grigorykhait.gkreader.logic.documents;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.grigorykhait.gkreader.logic.DocumentInvalidException;
import com.grigorykhait.gkreader.logic.nodes.fb2.AnnotationNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.BinaryNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.RootNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.SectionNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.content.ImageNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.description.DescriptionNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.description.TitleInfoNode;
import com.grigorykhait.gkreader.logic.parsers.FictionBookParser;
import com.grigorykhait.gkreader.logic.toc.TreeToc;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class FictionBookDocument extends Document {

    private static final String LOG_TAG = FictionBookDocument.class.getSimpleName();

    FictionBookParser mParser;
    String mRepresentation;
    private RootNode mRootNode;
    private TreeToc mToc;

    public FictionBookDocument(String filePath) throws IOException {
        this(filePath, false);
    }

    public FictionBookDocument(String filePath, boolean descriptionOnly) throws IOException {
        super(filePath);
        InputStream in;
        String extension = filePath.substring(filePath.lastIndexOf(".") + 1);
        if (extension.equals("zip")) {
            ZipFile zipFile = new ZipFile(filePath);
            if (zipFile.size() == 0 || zipFile.size() > 1) {
                throw new IllegalArgumentException("");
            }
            ZipEntry zipEntry = zipFile.entries().nextElement();
            in = zipFile.getInputStream(zipEntry);
        } else {
            in = new FileInputStream(filePath);
        }
        Log.v(LOG_TAG, "Before parsing");
        mParser = new FictionBookParser(in, descriptionOnly);
    }

    @Override
    public synchronized void parse() throws DocumentInvalidException {
        mRootNode = (RootNode) mParser.parse();
    }

    public RootNode getRootNode() {
        return mRootNode;
    }

    protected void setRootNode(RootNode root) {
        mRootNode = root;
    }

    @Override
    public List<String> getGenres() {
        return getDescription().getTitleInfo().getGenres();
    }

    @Override
    public String getLanguageCode() {
        return getTitleInfo().getLang();
    }

    @Override
    public String getRepresentation() {
        if (mRepresentation == null) {
            mRepresentation = getRootNode().getRepresentation();
        }
        return mRepresentation;
    }

    @Override
    public String getSection(String sectionId) {

        return null;
    }

    @Override
    public void writeToFile(File file) {
        try {
            PrintStream ps = new PrintStream(file, "UTF-8");
            ps.print(PREPEND);
            getRootNode().write(ps);
            ps.print(APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private TreeToc createToc() {
        TreeToc.Node root = new TreeToc.Node(null, getTitle(), "root", "");
        TreeToc toc = new TreeToc(root);
        List<SectionNode> sections = mRootNode.getBody().getSections();
        for (int i = 0; i < sections.size(); i++) {
            SectionNode section = sections.get(i);
            section.setId(String.valueOf(i));
            root.addChild(createNodeFromSection(root, section));
        }
        return toc;
    }

    private TreeToc.Node createNodeFromSection(TreeToc.Node parent, SectionNode section) {
        TreeToc.Node node = new TreeToc.Node(parent, section.getRepresentation(), "section", section.getId());
        if (section.isLeaf())
            return node;
        List<SectionNode> sections = section.getSections();
        for (int i = 0, sectionsSize = sections.size(); i < sectionsSize; i++) {
            SectionNode subSection = sections.get(i);
            subSection.setId(section.getId() + "-" + i);
            node.addChild(createNodeFromSection(node, subSection));
        }
        return node;
    }

    @Override
    public TreeToc getToc() {
        if (mToc == null) {
            mToc = createToc();
        }
        return mToc;
    }

    @Override
    public List<Attachment> getAttachments() {
        List<Attachment> attachments = new ArrayList<>();
        List<BinaryNode> images = getRootNode().getImages();
        if (images == null) return attachments;
        for (BinaryNode image : images) {
            Attachment attachment = new Attachment();
            attachment.innerLoc = image.getId();
            attachment.type = image.getContentType();
            attachment.content = image.getDataStream();
            attachments.add(attachment);
        }
        return attachments;
    }

    @Override
    public String getTitle() {
        return getDescription().getTitleInfo().getBookTitle();
    }

    @Override
    public Bitmap getCover() {
        List<ImageNode> coverNode = getTitleInfo().getCovers();
        if (coverNode == null) {
            return null;
        }
        BinaryNode image = getRootNode().findImageById(coverNode.get(0).getHref());
        if (image == null) {
            return null;
        }
        byte[] imageData = image.getData();
        return BitmapFactory.decodeByteArray(imageData, 0, imageData.length);
    }

    @Override
    public String getAnnotation() {
        AnnotationNode a = getTitleInfo().getAnnotation();
        if (a != null) {
            return a.getRepresentation();
        }
        return "";
    }

    @Override
    public List<String> getAuthors() {
        return getDescription().getTitleInfo().getAuthors();
    }

    private DescriptionNode getDescription() {
        return mRootNode.getDescription();
    }

    private TitleInfoNode getTitleInfo() {
        return getDescription().getTitleInfo();
    }
}
