package com.grigorykhait.gkreader.logic.documents;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;

import com.grigorykhait.gkreader.logic.DocumentInvalidException;
import com.grigorykhait.gkreader.logic.nodes.epub.ncx.NavPointNode;
import com.grigorykhait.gkreader.logic.nodes.epub.ncx.NcxNode;
import com.grigorykhait.gkreader.logic.nodes.epub.ocf.ContainerNode;
import com.grigorykhait.gkreader.logic.nodes.epub.opf.ManifestNode;
import com.grigorykhait.gkreader.logic.nodes.epub.opf.PackageNode;
import com.grigorykhait.gkreader.logic.nodes.epub.opf.SpineNode;
import com.grigorykhait.gkreader.logic.toc.TreeToc;

import org.jsoup.Jsoup;
import org.jsoup.parser.Parser;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.strategy.TreeStrategy;

import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class EpubDocument extends Document {

    public static final String CONTAINER_PATH = "META-INF/container.xml";
    private static final String LOG_TAG = EpubDocument.class.getSimpleName();

    private static final String OPF_TYPE = "application/oebps-package+xml";
    private static final String XHTML_TYPE = "application/xhtml+xml";


    private TreeToc mToc;
    private ContainerNode mContainer;
    private PackageNode mPackage;
    private String mOpfPath;

    public EpubDocument(String filePath) {
        super(filePath);
    }

    private static String getAbsolutePath(String relativePath, String packagePath) {
        File packageFile = new File(packagePath);
        File folder = new File(packageFile.getParent());
        File abs = new File(folder, relativePath);
        return abs.getPath().replace("\\", "/");
    }

    @Override
    public void parse() throws DocumentInvalidException {
        try {
            InputStream in;
            ZipFile zipFile = new ZipFile(getFile());
            if (zipFile.size() == 0) {
                throw new IllegalArgumentException("");
            }
            //           Log.v(LOG_TAG, "Before parsing container");
            ContainerNode container = parseContainer(zipFile);
            mOpfPath = container.getDefaultRootfile();
            PackageNode packageNode = parsePackage(zipFile, mOpfPath);
            mContainer = container;
            mPackage = packageNode;
            if (packageNode.getVersion().equals("2.0")) {
                String tocRelativePath = packageNode.getTocPath();

                NcxNode ncx = parseNcx(zipFile, getAbsolutePath(tocRelativePath, mOpfPath));
                mToc = createToc(ncx);
            }
        } catch (Exception e) {
            throw new DocumentInvalidException(e);
        }
    }

    private ContainerNode parseContainer(ZipFile zipFile) throws Exception {
        ZipEntry zipEntry = zipFile.getEntry(CONTAINER_PATH);
        InputStream in = zipFile.getInputStream(zipEntry);
        Serializer serializer = new Persister();
        ContainerNode root;
        root = serializer.read(ContainerNode.class, in, false);
        return root;
    }

    private PackageNode parsePackage(ZipFile zipFile, String filePath) throws Exception {
        ZipEntry zipEntry = zipFile.getEntry(filePath);
        InputStream in = zipFile.getInputStream(zipEntry);
        Serializer serializer = new Persister();
        PackageNode root;
        root = serializer.read(PackageNode.class, in, false);
        in.close();
        return root;
    }

    private NcxNode parseNcx(ZipFile zipFile, String filePath) throws Exception {
        ZipEntry zipEntry = zipFile.getEntry(filePath);
        InputStream in = zipFile.getInputStream(zipEntry);
        TreeStrategy strategy = new TreeStrategy("clazz", "len");
        Serializer serializer = new Persister(strategy);
        NcxNode ncx = serializer.read(NcxNode.class, in, false);
        in.close();
        return ncx;
    }

    private TreeToc createToc(NcxNode ncx) {
        TreeToc.Node root = new TreeToc.Node(null, ncx.getTitle(), "title", "");
        TreeToc toc = new TreeToc(root);
        for (NavPointNode point : ncx.getNavMap().getNavPoints()) {
            root.addChild(createNodeFromPoint(root, point));
        }
        return toc;
    }

    private TreeToc.Node createNodeFromPoint(TreeToc.Node parent, NavPointNode point) {
        TreeToc.Node node = new TreeToc.Node(parent, point.getLabel(), point.getClassText(), point.getContent());
        if (point.getNavPoints() != null) {
            for (NavPointNode subPoint : point.getNavPoints()) {
                node.addChild(createNodeFromPoint(node, subPoint));
            }
        }
        return node;
    }

    @NonNull
    @Override
    public String getTitle() {
        return mPackage.getMetadata().getTitle();
    }

    @Override
    public Bitmap getCover() {
        String coverPath = mPackage.getCoverPath();
        if (coverPath == null) return null;
        String fullPath = getAbsolutePath(coverPath, mOpfPath);
        try {
            ZipFile zipFile = new ZipFile(getFile());
            ZipEntry entry = zipFile.getEntry(fullPath);
            return BitmapFactory.decodeStream(zipFile.getInputStream(entry));
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public String getAnnotation() {
        return mPackage.getMetadata().getDescription();
    }

    @Override
    public List<String> getAuthors() {
        return mPackage.getMetadata().getAuthors();
    }

    @Override
    public List<String> getGenres() {
        return mPackage.getMetadata().getSubjects();
    }

    @Override
    public String getLanguageCode() {
        return mPackage.getMetadata().getLanguage();
    }

    @Override
    public String getRepresentation() {
        return null;
    }

    @Override
    public String getSection(String sectionId) throws DocumentInvalidException {
        ManifestNode.ItemNode section = mPackage.getManifest().findNodeById(sectionId);
        String path = getAbsolutePath(section.href, mOpfPath);
        try {
            ZipFile zipFile = new ZipFile(getFile());
            ZipEntry entry = zipFile.getEntry(path);
            InputStream in = zipFile.getInputStream(entry);
            return new DataInputStream(in).readUTF();
        } catch (IOException e) {
            throw new DocumentInvalidException(e);
        }
    }

    @Override
    public void writeToFile(File file) {
        try {
            PrintStream ps = new PrintStream(file, "UTF-8");
            ps.print(PREPEND);
            ZipFile zipFile = new ZipFile(getFile());
            for (SpineNode.ItemRefNode node : mPackage.getSpine().getItemRefs()) {
                ManifestNode.ItemNode item = mPackage.getManifest().findNodeById(node.idref);
                ZipEntry entry = zipFile.getEntry(getAbsolutePath(item.href, mOpfPath));
                org.jsoup.nodes.Document document = Jsoup.parse(zipFile.getInputStream(entry), null, "", Parser.xmlParser());
                ps.print(document.body().html());
            }
            zipFile.close();
            ps.print(APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public TreeToc getToc() {
        return mToc;
    }

    @Override
    public List<Attachment> getAttachments() {
        List<Attachment> attachments = new ArrayList<>();
        try {
            ZipFile zipFile = new ZipFile(getFile());
            for (ManifestNode.ItemNode item : mPackage.getManifest().getNodes()) {
                if (!item.mediaType.equals(XHTML_TYPE) && !item.mediaType.equals(OPF_TYPE)) {
                    ZipEntry entry = zipFile.getEntry(getAbsolutePath(item.href, mOpfPath));
                    Attachment attachment = new Attachment();
                    attachment.type = item.mediaType;
                    attachment.innerLoc = item.href;
                    attachment.content = zipFile.getInputStream(entry);
                    attachments.add(attachment);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return attachments;
    }

}
