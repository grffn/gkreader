package com.grigorykhait.gkreader.logic.nodes.atom;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Text;

public class TextConstructNode {

    public final static String TYPE_TEXT = "text";
    public final static String TYPE_HTML = "html";
    public final static String TYPE_XHTML = "xhtml";

    @Element
    private String type;

    @Element(required = false)
    private String div;

    @Text(required = false)
    private String text;
}
