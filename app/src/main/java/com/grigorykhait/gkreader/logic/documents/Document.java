/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Grigory Khait
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.grigorykhait.gkreader.logic.documents;


import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.grigorykhait.gkreader.logic.DocumentInvalidException;
import com.grigorykhait.gkreader.logic.toc.TreeToc;

import java.io.File;
import java.io.InputStream;
import java.util.List;

public abstract class Document {

    protected static final String PREPEND = "<html><head>" +
            "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">" +
            "<meta charset=\"UTF-8\">" +
            "<link href=\"file:///android_asset/normalize.css\" type=\"text/css\" rel=\"stylesheet\">" +
            "<link href=\"file:///android_asset/main.css\" type=\"text/css\" rel=\"stylesheet\">" +
            "</head><body>" +
            "<div id=\"gkreader_wrapper\"><div id='gkreader_header'></div><div id=\"gkreader_container\">";
    protected static final String APPEND = "</div></div>" +
            "<script src=\"file:///android_asset/raphael-min.js\" type=\"text/javascript\">" +
            "</script><script src=\"file:///android_asset/main.js\" type=\"text/javascript\"></script>" +
            "</body></html>";

    private File mFile;

    protected Document(String filePath) {
        mFile = new File(filePath);
    }

    public abstract void parse() throws DocumentInvalidException;

    @NonNull
    public abstract String getTitle();

    public abstract Bitmap getCover();

    public abstract String getAnnotation();

    public abstract List<String> getAuthors();

    @NonNull
    public File getFile() {
        return mFile;
    }

    public abstract List<String> getGenres();

    public abstract String getLanguageCode();

    public abstract String getRepresentation();

    public abstract String getSection(String sectionId) throws DocumentInvalidException;

    public abstract void writeToFile(File file);

    public abstract TreeToc getToc();

    public abstract List<Attachment> getAttachments();

    public static class Attachment {

        public String type;

        public InputStream content;

        public String innerLoc;
    }
}
