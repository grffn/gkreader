package com.grigorykhait.gkreader.logic.nodes.epub.ncx;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;

import java.util.List;

public class PageListNode {


    @Path("navInfo/text")
    @ElementList(required = false)
    private List<String> info;

    @Path("navLabel/text")
    @ElementList
    private List<String> labels;


}
