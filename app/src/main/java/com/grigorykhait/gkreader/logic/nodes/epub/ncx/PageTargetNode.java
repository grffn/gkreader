package com.grigorykhait.gkreader.logic.nodes.epub.ncx;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;

import java.util.List;

public class PageTargetNode {

    @Attribute
    private String id;

    @Attribute(required = false, name = "class")
    private String classText;

    @Attribute(required = false)
    private String value;

    @Attribute(required = false)
    private String playOrder;

    @Attribute
    private String type;

    @Path("navLabel/text")
    @ElementList
    private List<String> label;

    @Attribute(required = false)
    @Path("content[@src]")
    private String content;

}
