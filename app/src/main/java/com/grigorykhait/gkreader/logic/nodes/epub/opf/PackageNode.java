package com.grigorykhait.gkreader.logic.nodes.epub.opf;

import android.content.ClipData;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.util.NoSuchElementException;

@Root(name = "package", strict = false)
public class PackageNode {

    @Attribute
    private String version;

    @Element
    private MetadataNode metadata;

    @Element
    private ManifestNode manifest;

    @Element
    private SpineNode spine;

    public String getVersion() {
        return version;
    }

    public MetadataNode getMetadata() {
        return metadata;
    }

    public ManifestNode getManifest() {
        return manifest;
    }

    public SpineNode getSpine() {
        return spine;
    }

    public String getTocPath() {
        String ncxId = spine.getToc();
        ManifestNode.ItemNode ncx = manifest.findNodeById(ncxId);
        return ncx.href;
    }

    public String getCoverPath() {
        try {
            String coverId = metadata.getCoverId();
            ManifestNode.ItemNode cover = manifest.findNodeById(coverId);
            return cover.href;
        } catch (NoSuchElementException exc) {
            return null;
        }

    }

}
