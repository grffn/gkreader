/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Grigory Khait
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.grigorykhait.gkreader.logic.nodes.fb2;

import com.grigorykhait.gkreader.logic.nodes.fb2.content.CiteNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.content.ContentNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.content.EmptyLineNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.content.ImageNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.content.PNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.content.PoemNode;
import com.grigorykhait.gkreader.logic.nodes.fb2.content.TableNode;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.ElementListUnion;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;

/**
 * Created by grffn on 15.03.2015.
 */
public class SectionNode extends Fb2Node {

    @Attribute(required = false, name = "id")
    private String id;

    @Element(required = false)
    private TitleNode title;

    @ElementList(required = false, inline = true)
    private List<EpigraphNode> epigraphList;

    //@Element(required = false)
    //private ImageNode image;
    @Element(required = false)

    private AnnotationNode annotation;
    @ElementList(entry = "section", inline = true, required = false)
    private List<SectionNode> sections;
    @ElementListUnion({
            @ElementList(entry = "p", inline = true, type = PNode.class, required = false),
            @ElementList(entry = "image", inline = true, type = ImageNode.class, required = false),
            @ElementList(entry = "cite", inline = true, type = CiteNode.class, required = false),
            @ElementList(entry = "empty-line", inline = true, type = EmptyLineNode.class, required = false),
            @ElementList(entry = "subtitle", inline = true, type = PNode.class, required = false),
            @ElementList(entry = "table", inline = true, type = TableNode.class, required = false),
            @ElementList(entry = "poem", inline = true, type = PoemNode.class, required = false),
    })
    private List<ContentNode> contentNodes;

    public List<SectionNode> getSections() {
        return sections;
    }

    @Override
    public String getRepresentation() {
        StringBuilder builder = new StringBuilder();
        if (!isLeaf()) {
            for (SectionNode section : sections) {
                builder.append(section.getRepresentation());
            }
        } else {
            for (ContentNode contentNode : contentNodes) {
                builder.append(contentNode.getRepresentation());
            }
        }
        return builder.toString();
    }

    public boolean isLeaf() {
        return sections == null || sections.size() == 0;
    }

    public String getTitle() {
        if (title == null)
            return "";
        return title.getRepresentation();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void write(PrintStream out) throws IOException {
        if (!isLeaf()) {
            for (SectionNode section : sections) {
                section.write(out);
            }
        } else {
            for (ContentNode contentNode : contentNodes) {
                contentNode.write(out);
            }
        }
    }
}
