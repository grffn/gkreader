/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Grigory Khait
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.grigorykhait.gkreader.logic.parsers;

import com.grigorykhait.gkreader.logic.nodes.Node;
import com.grigorykhait.gkreader.logic.nodes.txt.RootNode;
import com.grigorykhait.gkreader.logic.nodes.txt.TextNode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

/**
 * Created by Григорий on 28.12.2014.
 */
public class PlainTextParser extends Parser {
    public static final String NEW_LINE = System.getProperty("line.separator");

    private State mState = State.INITIAL;
    private BufferedReader mReader;

    public PlainTextParser(Reader reader) {
        mReader = new BufferedReader(reader);
    }

    @Override
    public synchronized Node parse() throws IOException {
        if (mState != State.INITIAL) {
            throw new UnsupportedOperationException("Parser is in wrong state: " + mState);
        }
        mState = State.PARSING;
        String line;
        RootNode rootNode = new RootNode();
        try {
            while ((line = mReader.readLine()) != null) {
                if (line.isEmpty()) {
                    continue;
                }
                TextNode textNode = new TextNode(line);
                rootNode.addChild(textNode);
            }
            mState = State.PARSED;
        } catch (IOException e) {
            e.printStackTrace();
            mState = State.ERROR;
            throw e;
        }
        return rootNode;
    }


}
