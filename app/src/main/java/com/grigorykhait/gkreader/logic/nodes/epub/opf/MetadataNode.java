package com.grigorykhait.gkreader.logic.nodes.epub.opf;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Text;

import java.util.ArrayList;
import java.util.List;

public class MetadataNode {

    @ElementList(entry = "title", inline = true)
    private List<TitleNode> titles;

    @ElementList(entry = "identifier", inline = true)
    private List<String> identifiers;

    @ElementList(entry = "language", inline = true)
    private List<String> languages;

    @ElementList(entry = "contributor", required = false, inline = true)
    private List<CreatorNode> contributors;

    @ElementList(entry = "coverage", required = false, inline = true)
    private List<String> coverage;

    @ElementList(entry = "creator", required = false, inline = true)
    private List<CreatorNode> creators;

    @ElementList(entry = "date", required = false, inline = true)
    private List<String> dates;
    @ElementList(entry = "description", required = false, inline = true)
    private List<String> descriptions;
    @ElementList(entry = "format", required = false, inline = true)
    private List<String> format;
    @ElementList(entry = "publisher", required = false, inline = true)
    private List<String> publisher;
    @ElementList(entry = "relation", required = false, inline = true)
    private List<String> relation;
    @ElementList(entry = "rights", required = false, inline = true)
    private List<String> rights;
    @ElementList(entry = "source", required = false, inline = true)
    private List<String> source;
    @ElementList(entry = "subject", required = false, inline = true)
    private List<String> subjects;
    @ElementList(entry = "type", required = false, inline = true)
    private List<String> type;
    @ElementList(entry = "meta", required = false, inline = true)
    private List<MetaNode> metas;

    public List<TitleNode> getTitles() {
        return titles;
    }

    public String getTitle() {
        return titles.get(0).title;
    }

    public String getDescription() {
        if (descriptions == null || descriptions.size() > 0)
            return "";
        return descriptions.get(0);
    }

    public List<String> getLanguages() {
        return languages;
    }

    public String getLanguage() {
        return languages.get(0);
    }

    public List<CreatorNode> getCreators() {
        return creators;
    }

    public List<String> getAuthors() {
        ArrayList<String> authors = new ArrayList<>();
        if (creators != null) {
            for (CreatorNode creator : creators) {
                authors.add(creator.title);
            }
        }
        return authors;
    }

    public List<String> getSubjects() {
        return subjects;
    }

    public List<String> getDates() {
        return dates;
    }

    public List<String> getDescriptions() {
        return descriptions;
    }

    public String getCoverId() {
        if (metas == null) return null;
        for (MetaNode meta : metas) {
            if (meta.name.equals("cover")) {
                return meta.contentAttr;
            }
        }
        return null;
    }

    private static class TitleNode {

        @Attribute(required = false)
        public String id;

        @Attribute(required = false)
        public String lang;

        @Attribute(required = false)
        public String dir;

        @Text
        public String title;

    }

    private static class CreatorNode {

        @Attribute(name = "file-as", required = false)
        public String fileAs;

        @Attribute(required = false)
        public String role;

        @Text
        public String title;
    }

    public static class MetaNode {

        @Attribute(required = false)
        public String property;

        @Attribute(required = false)
        public String refines;

        @Attribute(required = false)
        public String id;

        @Attribute(required = false)
        public String scheme;

        @Attribute(required = false)
        public String dir;

        @Text(required = false)
        public String content;

        @Attribute(required = false)
        public String name;

        @Attribute(name = "content", required = false)
        public String contentAttr;

    }
}
