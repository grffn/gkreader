package com.grigorykhait.gkreader.logic.nodes.fb3.descr;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import java.util.List;

public class ObjectRelationNode {
    @Element
    public TitleNode title;


    @Element(required = false)
    public String description;

    @Attribute
    public String id;

    @Attribute
    public String link;

    public class ObjectRelationEnum {

    }
}
