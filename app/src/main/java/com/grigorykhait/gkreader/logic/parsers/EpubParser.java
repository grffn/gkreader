package com.grigorykhait.gkreader.logic.parsers;

import com.grigorykhait.gkreader.logic.DocumentInvalidException;
import com.grigorykhait.gkreader.logic.nodes.Node;
import com.grigorykhait.gkreader.logic.nodes.epub.ocf.ContainerNode;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by grffn on 29.04.2015.
 */
public class EpubParser {

    private static final String LOG_TAG = EpubParser.class.getSimpleName();

    InputStream in;
    boolean descriptionOnly;

    public EpubParser(InputStream in, boolean descriptionOnly) {
        this.in = in;
        this.descriptionOnly = descriptionOnly;
    }

    public ContainerNode parse() throws DocumentInvalidException {
        try {
            Serializer serializer = new Persister();
            ContainerNode root;
            root = serializer.read(ContainerNode.class, in, false);
            return root;
        } catch (Exception e) {
            e.printStackTrace();
            throw new DocumentInvalidException(e);
        }
    }
}
