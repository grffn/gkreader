package com.grigorykhait.gkreader.logic;

import com.grigorykhait.gkreader.logic.documents.Document;
import com.grigorykhait.gkreader.logic.documents.EpubDocument;
import com.grigorykhait.gkreader.logic.documents.FictionBookDocument;
import com.grigorykhait.gkreader.logic.documents.PlainTextDocument;

import java.io.IOException;

/*
only for testing purposes
*/
class DesktopLauncher {
    private static String mFilePath = "F:\\Books & etc\\1421.epub";

    public static void main(String... args) throws IOException, DocumentInvalidException {
        Document document = null;
        if (mFilePath.endsWith(".txt")) {
            document = new PlainTextDocument(mFilePath);
        } else if (mFilePath.endsWith(".fb2") || mFilePath.endsWith(".fb2.zip")) {
            document = new FictionBookDocument(mFilePath);
        } else if (mFilePath.endsWith(".epub")) {
            document = new EpubDocument(mFilePath);
        }
        document.parse();
        document.getCover();
    }
}
