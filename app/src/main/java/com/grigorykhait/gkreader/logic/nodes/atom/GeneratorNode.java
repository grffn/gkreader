package com.grigorykhait.gkreader.logic.nodes.atom;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Text;

public class GeneratorNode {

    @Element(required = false)
    private String uri;

    @Element(required = false)
    private String version;

    @Text
    private String content;
}
